<?php

namespace Drupal\teamtailor\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\teamtailor\TeamtailorController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of candidates for jobs.
 */
class CandidatesListController extends ControllerBase {

  /**
   * The Teamtailor controller.
   *
   * @var \Drupal\teamtailor\TeamtailorController
   */
  protected $teamtailorController;

  /**
   * Constructs a new CandidatesListController object.
   *
   * @param \Drupal\teamtailor\TeamtailorController $teamtailorController
   *   The Teamtailor controller.
   */
  public function __construct(TeamtailorController $teamtailorController) {
    $this->teamtailorController = $teamtailorController;
  }

  /**
   * Creates an instance of the controller with dependencies injected.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return static
   *   A new instance of this class.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('teamtailor.controller')
    );
  }

  /**
   * Displays a list of candidates for jobs.
   *
   * @return array
   *   A render array for a candidates page.
   */
  public function content() {
    $data = [];
    $job_data = $this->teamtailorController->getData();

    foreach ($job_data as $key => $job) {
      $data[$key]['job'] = [
        'id' => $key,
        'title' => $job['job']['title'],
      ];
      foreach ($job['candidates'] as $candidate_data) {
        $data[$key]['candidates'][] = $candidate_data;
      }
    }

    return [
      '#theme' => 'candidates_page',
      '#data' => $data,
    ];
  }

}
