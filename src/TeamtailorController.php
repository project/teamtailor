<?php

namespace Drupal\teamtailor;

use Drupal\Core\Config\ConfigFactory;

/**
 * Provides functionality to interact with the Teamtailor API.
 */
class TeamtailorController {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  private $apiKey;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $client;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a new MyService object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->configFactory = $configFactory;
    $this->setApiKey();
  }

  /**
   * Sets the API key and version from the configuration.
   */
  private function setApiKey() {
    $this->apiKey = $this->configFactory->get('teamtailor.settings')->get('api_key');
    $this->apiVersion = $this->configFactory->get('teamtailor.settings')->get('api_version');
  }

  /**
   * Retrieves data from the Teamtailor API.
   */
  public function getData() {
    $jobs = $this->getJobs();
    $jobs_data = [];
    foreach ($jobs->data as $job) {
      $job_object = (array) $this->getJob($job->id)->data->attributes;
      $jobs_data[$job->id]['job'] = $job_object;
      $candidates_data = $this->getCandidates($job->id);
      $candidate = [];
      foreach ($candidates_data->data as $cand) {
        $candidate[$cand->id] = (array) $cand->attributes;
        $role = $this->getUserRole($cand->id);
        $candidate[$cand->id]['role'] = $role;

      }
      $jobs_data[$job->id]['candidates'] = $candidate;
    }
    return $jobs_data;
  }

  /**
   * Retrieves the user role based on the provided ID.
   *
   * @param int $id
   *   The user ID.
   *
   * @return array
   *   The user role information.
   */
  public function getUserRole($id) {
    $url = 'https://api.teamtailor.com/v1/candidates/' . $id . '/relationships/role';

    return $this->apiCall($url);
  }

  /**
   * Retrieves candidates based on the provided job ID.
   *
   * @param int|bool $job_id
   *   The job ID or FALSE to retrieve all candidates.
   *
   * @return array
   *   The candidates information.
   */
  public function getCandidates($job_id = FALSE) {
    if ($job_id) {
      $url = 'https://api.teamtailor.com/v1/jobs/' . $job_id . '/candidates';
    }
    else {
      $url = 'https://api.teamtailor.com/v1/candidates';
    }
    return $this->apiCall($url);
  }

  /**
   * Retrieves the job details based on the provided ID.
   *
   * @param int $id
   *   The job ID.
   *
   * @return array
   *   The job details.
   */
  public function getJob($id) {
    $url = 'https://api.teamtailor.com/v1/jobs/' . $id;

    return $this->apiCall($url);
  }

  /**
   * Retrieves all jobs.
   *
   * @todo Complete this.
   *
   * @return array
   *   The jobs information.
   */
  public function getJobs() {
    $url = "https://api.teamtailor.com/v1/jobs?include=department,locations";

    return $this->apiCall($url);
  }

  /**
   * Api call.
   */
  private function apiCall($url, $method = 'GET') {
    try {
      $path = $url;
      $request = $this->client->request($method, $path, [
        'headers' => [
          'Authorization' => 'Token token=' . $this->apiKey,
          'X-Api-Version' => $this->apiVersion,
        ],
      ]);
      $file_contents = $request->getBody()->getContents();
      $result = json_decode($file_contents);
    }
    catch (ClientException $exception) {
      $result = [];
    }
    return $result;
  }

}
