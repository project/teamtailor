<?php

namespace Drupal\teamtailor\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a settings form for the Teamtailor module.
 */
class TeamtailorSettings extends ConfigFormBase {

  const SETTINGS = 'teamtailor.settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'teamtailor_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('api_key', $form_state->getValue('teamtailor_api_key'))
      ->set('api_version', $form_state->getValue('teamtailor_api_version'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Defines the settings form for Teamtailor entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['teamtailor_settings']['#markup'] = $this->t('Settings form for Teamtailor Api.');
    $form['teamtailor_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Teamtailor api key'),
      '#default_value' => $config->get('api_key'),
    ];
    $form['teamtailor_api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Teamtailor api version'),
      '#default_value' => $config->get('api_version') ? $config->get('api_version') : '20161108',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

}
